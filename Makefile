OS:=$(shell lsb_release -si)

ifeq (, $(shell command -v stow))
$(warning "No stow command in $(PATH), consider installing stow tool or use 'make system' command")
endif
ifeq (, $(shell command -v ansible-playbook))
$(warning "No ansible-playbook command in $(PATH), consider installing ansible tool")
endif

ifeq ($(VERBOSE), 1)
  VERBOSE_OPT=-vvv
else
  VERBOSE_OPT=
endif

BECOME_METHOD=sudo

user:
	ansible-playbook -i hosts.yml desktop.yml --skip-tags become $(VERBOSE_OPT)
all:
	ansible-playbook -i hosts.yml desktop.yml --become-method=$(BECOME_METHOD) -K $(VERBOSE_OPT)
test:
	ansible-playbook -i hosts.yml desktop.yml --become-method=$(BECOME_METHOD) -K --tags test $(VERBOSE_OPT)
check:
	ansible-playbook -i hosts.yml desktop.yml --tags check $(VERBOSE_OPT)
nix:
	nix-env -if home.nix

.PHONY: check user system test nix
