#!/usr/bin/env python3

import sys, re
import requests
import yaml
import argparse
import asyncio

parser = argparse.ArgumentParser(description='Various checks around github projects releases.')
parser.add_argument('--display', '-d', action='append', nargs=1, metavar='<user>/<repository_name>', help='display the last release version')
parser.add_argument('--check-version', '-c', action='append', nargs=2, metavar=('<user>/<repository_name> or project URL', '<version>'), help='Compare last release and given version')
parser.add_argument('--check-yaml', '-y', action='append', metavar=('<user>/<repository_name>', '<yaml_file>', '<yaml_app_path>'), nargs=3, help='Compare last release and version included in the ansible yaml file')

github_root_url="https://github.com/"

def get_latest_release_tag(repo_relative_url):
    api = "https://api.github.com/repos/%s/releases/latest" % repo_relative_url
    r = requests.get(api)
    j = r.json()
    if 'tag_name' in j.keys():
        return j['tag_name']
    # no release ? try the tags
    api = "https://api.github.com/repos/%s/tags" % repo_relative_url
    r = requests.get(api)
    j = r.json()
    if isinstance(j, list) and len(j) > 0:
        return j[0]['name']
    raise Exception("Cannot find release for %s: %s" % (repo_relative_url, str(j)))

def go_through(dictionary, path):
    current = dictionary
    for item in path.split('.'):
        if item not in current:
            return None
        current = current[item]
    return current

def get_version_from_yaml(yaml_filename, yaml_path):
    with open(yaml_filename) as f:
        doc = yaml.load(f, Loader=yaml.BaseLoader)
        return go_through(doc, yaml_path)

async def get_repos_latest_release_tag(repos):
    """Launch the get_latest_release_tag function asynchronously for a list of repositories"""
    # get_event_loop will get the current loop or create one
    loop = asyncio.get_event_loop()
    futures = {}
    result = {}
    for repo in repos:
        # run_in_executor with first argument as None will use the default Thread pool
        futures[repo] = loop.run_in_executor(None, get_latest_release_tag, repo)
    for repo, future in futures.items():
        result[repo] = await future
    return result

def get_release_url_from_repo(repo):
    return "https://github.com/%s/releases" % repo

if __name__ == '__main__':
    parsed = parser.parse_args()
    return_code = 0
    # get all repositories to check
    repos = []
    if parsed.display:
        for repo in parsed.display:
            repos.append(repo[0])
    if parsed.check_version:
        for repo, version_to_check in parsed.check_version:
            if repo.startswith(github_root_url):
                repo = repo[len(github_root_url):]
            repos.append(repo)
    if parsed.check_yaml:
        for repo, yaml_filename, yaml_path in parsed.check_yaml:
            repos.append(repo)
    # use asyncio to:
    # - get an event loop
    # - launch the requests in a thread pool
    loop = asyncio.get_event_loop()
    tags = loop.run_until_complete(get_repos_latest_release_tag(repos))
    if parsed.display:
        for repo in parsed.display:
            last_tag = tags[repo[0]]
            print(last_tag, end='')
    if parsed.check_version:
        for repo, version_to_check in parsed.check_version:
            if repo.startswith(github_root_url):
                repo = repo[len(github_root_url):]
            last_tag = tags[repo]
            if last_tag != version_to_check:
                print("Project %s has a new version! %s has been released (given: %s). Check %s ." % (repo, last_tag, version_to_check, get_release_url_from_repo(repo)))
                return_code = 1
    if parsed.check_yaml:
        for repo, yaml_filename, yaml_path in parsed.check_yaml:
            last_tag = tags[repo]
            version_to_check = get_version_from_yaml(yaml_filename, yaml_path)
            if last_tag != version_to_check:
                print("Project %s has a new version! %s has been released (installed: %s). Check %s ." % (repo, last_tag, version_to_check, get_release_url_from_repo(repo)))
                return_code = 1
    sys.exit(return_code)

