# Home sweet home

dotfiles and tracks of desktop environment experiments.

## Requirements

- `make`
- `ansible`

## Installation

- `make user` to install userland config
- `make system` to install system-wide applications
- `make check` to check installed app github new releases

## Note on stow and dotfiles

Inspired from [this guide](http://brandon.invergo.net/news/2012-05-26-using-gnu-stow-to-manage-your-dotfiles.html).

- install the GNU `stow` package
- launch `make <tool_directory>` for each tool configuration you want to install in the `dotfiles` directory. For example:
  ```
  ~/homesweethome/dotfiles$ make zsh
  ```
  will generate all the symlinks that refer to the files inside the `zsh` directory

