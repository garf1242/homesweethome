#!/bin/bash
 
case "$1" in
    raise)
        pactl set-sink-volume @DEFAULT_SINK@ "+5%"
        ;;
    lower)
        pactl set-sink-volume @DEFAULT_SINK@ "-5%"
        ;;
    toggle)
        pactl set-sink-mute @DEFAULT_SINK@ toggle
        ;;
    mictoggle)
        pactl set-source-mute @DEFAULT_SOURCE@ toggle
        ;;
esac
