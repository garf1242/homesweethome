#!/bin/bash -x

second_monitor_workspace="10"

first_monitor=`xrandr -q | grep " connected" | grep "primary" | awk '{print $1}' | head -n 1`
second_monitor=`xrandr -q | grep " connected" | grep -v "primary" | awk '{print $1}' | head -n 1`

if [ "x$second_monitor" == "x" ]
then
    xrandr --auto
    exit 0
fi


xrandr --output ${second_monitor} --auto --left-of ${first_monitor}

i3-msg "workspace ${second_monitor_workspace}, move workspace to output ${second_monitor}"

