#!/bin/bash
 
DEFAULTSINK=`pacmd stat | sed -n -e "s/^Default sink name: \(.*\)/\1/p"`
DEFAULTSOURCE=`pacmd stat | sed -n -e "s/^Default source name: \(.*\)/\1/p"`

case "$1" in
    raise)
        pactl set-sink-volume "$DEFAULTSINK" "+5%"
        ;;
    lower)
        pactl set-sink-volume "$DEFAULTSINK" "-5%"
        ;;
    toggle)
        pactl set-sink-mute "$DEFAULTSINK" toggle
        ;;
    mictoggle)
        pactl set-source-mute "$DEFAULTSOURCE" toggle
        ;;
esac
