# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
#if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
#fi

# ZSH OPTIONS
setopt ALWAYS_TO_END           # full completions move cursor to the end
setopt AUTO_CD                 # `dirname` is equivalent to `cd dirname`
setopt AUTO_LIST               # automatically list choices on ambiguous completion
unsetopt AUTO_MENU               # show completion menu on a successive tab press
setopt AUTO_PARAM_SLASH        # if completed parameter is a directory, add a trailing slash
setopt AUTO_PUSHD              # `cd` pushes directories to the directory stack
setopt COMPLETE_IN_WORD        # complete from the cursor rather than from the end of the word
setopt EXTENDED_GLOB           # (#qx) glob qualifier and more
setopt GLOB_DOTS               # glob matches files starting with dot; `*` becomes `*(D)`
setopt INTERACTIVE_COMMENTS    # allow comments in command line
setopt MULTIOS                 # allow multiple redirections for the same fd
setopt NO_BG_NICE              # don't nice background jobs; not useful and doesn't work on WSL
setopt NO_FLOW_CONTROL         # disable start/stop characters in shell editor
setopt NO_MENU_COMPLETE        # do not autoselect the first completion entry
setopt PATH_DIRS               # perform path search even on command names with slashes
setopt NO_HUP                  # background jobs are not killed when the shell exits
setopt EXTENDED_HISTORY        # write timestamps to history
setopt HIST_EXPIRE_DUPS_FIRST  # if history needs to be trimmed, evict dups first
setopt HIST_FIND_NO_DUPS       # don't show dups when searching history
setopt HIST_IGNORE_ALL_DUPS    # don't add dups to history
setopt HIST_IGNORE_SPACE       # don't add commands starting with space to history
setopt HIST_REDUCE_BLANKS      # remove junk whitespace from commands before adding to history
setopt HIST_VERIFY             # if a command triggers history expansion, show it instead of running
setopt NO_BANG_HIST            # disable old history syntax
setopt SHARE_HISTORY           # write and import history on every command
#setopt INC_APPEND_HISTORY      # add commands to HISTFILE without waiting their return
#setopt INC_APPEND_HISTORY_TIME  # 

# deactivate the warning of too long list of suggestions...
LISTMAX=10000

eval "$(/usr/bin/dircolors)" # set default LS_COLORS

# Basic auto/tab complete:
#autoload -U compinit
#zstyle ':completion:*' menu no select # menu but no selection
#zstyle ':completion:*' menu select
#zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}" # use ls colors for completion
#zmodload zsh/complist
#compinit
#zstyle -e ':completion:*:default' list-colors 'reply=("${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=35}:${(s.:.)LS_COLORS}")' # color the prefix in suggestion list
source $HOME/.config/zsh/completion.zsh

# THEME: powerlevel10k
if [ -f /usr/local/share/zsh-powerlevel10k/powerlevel10k.zsh-theme ]; then
    source /usr/local/share/zsh-powerlevel10k/powerlevel10k.zsh-theme
else
    source /usr/share/zsh-theme-powerlevel10k/powerlevel10k.zsh-theme
fi
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh


# starship
#eval "$(starship init zsh)"


# don't leave an empty space after right prompt
ZLE_RPROMPT_INDENT=0

# Title
autoload -Uz add-zsh-hook
function xterm_title_precmd () {
	print -Pn -- '\e]2;%n@%m %~\a'
}
function xterm_title_preexec () {
	print -Pn -- '\e]2;%n@%m %~ %# ' && print -n -- "${(q)1}\a"
}
add-zsh-hook -Uz precmd xterm_title_precmd
add-zsh-hook -Uz preexec xterm_title_preexec

# History
HISTFILE="$HOME/.zsh_history"
HISTSIZE=10000
SAVEHIST=10000

# QT
# Don't want to scale based on DPI, just on resolution
export QT_AUTO_SCREEN_SCALE_FACTOR=0

# SSH and GPG
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

# PATH & co
export PATH=${HOME}/bin:/usr/local/bin:/usr/local/go/bin:${HOME}/go/bin:/snap/bin:${HOME}/.cargo/bin:${PATH}
# Do not add jdk lib path manually, use `archlinux-java` instead
export LD_LIBRARY_PATH=/usr/local/lib/:/usr/lib
export EDITOR=nvim

# denv
#export WORKSPACE=${HOME}/workspace
export DENV_COMPOSE_COMMAND="docker compose"
export DENV_COMPOSE_RUN_OPTIONS="--use-aliases"

# passk
passk completion-zsh | . /dev/stdin

# swaywm: fix java awt
export _JAVA_AWT_WM_NONREPARENTING=1
# android studio
android_studio_dir=$HOME/utils/android-studio
export PATH=$android_studio_dir/bin:$android_studio_dir/jre/bin:$PATH
# android sdk
export PATH=$HOME/Android/Sdk/platform-tools:$PATH

# Fzf
fd_command="fd"
if [ -x "$(command -v fdfind)" ]; then
    fd_command="fdfind"
fi

export FZF_DEFAULT_OPTS="--reverse --border --height=80%"
export FZF_DEFAULT_COMMAND="${fd_command} --type file --follow --hidden --exclude .git"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
#export FZF_ALT_C_COMMAND="${fd_command} . -I -t d"
# Install bfs from the AUR (breadth first version of find)
# https://github.com/tavianator/bfs
export FZF_ALT_C_COMMAND="bfs . -type d"
if [ -d /usr/local/src/fzf/shell ]; then
    source /usr/local/src/fzf/shell/completion.zsh
    source /usr/local/src/fzf/shell/key-bindings.zsh
else
    source /usr/share/fzf/completion.zsh
    source /usr/share/fzf/key-bindings.zsh
fi

# less
# This affects every invocation of `less`.
#
#   -i   case-insensitive search unless search string contains uppercase letters
#   -R   color
#   -X   keep content on screen after exit
#   -M   show more info at the bottom prompt line
#   -x4  tabs are 4 instead of 8
#   -F   automatically exit if content can be display within one screen
export LESS=-iRXMFx4

# Disable highlighting of text pasted into the command line.
zle_highlight=('paste:none')

# Aliases
source $HOME/.config/zsh/aliases.zsh

# default keybindings ?
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[3~" delete-char
# Ctrl+arrows to jump from word to word
bindkey "^[[1;5D" backward-word
bindkey "^[[1;5C" forward-word

# Edit line in vim with ctrl-e:
autoload edit-command-line; zle -N edit-command-line
bindkey '^e' edit-command-line

# activate extension in `pass`
export PASSWORD_STORE_ENABLE_EXTENSIONS="true"

# Denv path and completion
export PATH=$HOME/workspace/denv/bin:$PATH
source $HOME/workspace/denv/completion/denv.zsh

# zsh-autosuggestions
# source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

# rootless docker
export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock

# do not use any creds for python package retrieval
export PYTHON_KEYRING_BACKEND="keyring.backends.null.Keyring"

# zsh-syntax-highlighting; should be last.
if [ -f /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; then
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
else
    source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
fi
ZSH_HIGHLIGHT_STYLES[comment]='fg=245' # comments are black by default. display them in grey
