
alias ls='ls --color=auto --hyperlink=auto'
alias ll='ls -alh'

alias vlc3='/usr/bin/vlc'
export VLC_LOCAL_BUILD=$HOME/workspace/videolan/vlc/build
alias vlc4='LD_LIBRARY_PATH="" ${VLC_LOCAL_BUILD}/bin/vlc-static'

alias docker-clean-containers='docker ps -aq --no-trunc | xargs docker rm'

alias scan-boulot='hp-scan -mcolor --size=a4'

pdf-shrink() {
	local pdf_filename
	pdf_filename=$1
	pdf_filename_base=${pdf_filename%.pdf}
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH "-sOutputFile=${pdf_filename_base}-shrink.pdf" "${pdf_filename}"
}

pdf-shrink-ultra() {
	local pdf_filename
	pdf_filename=$1
	pdf_filename_base=${pdf_filename%.pdf}
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/screen -dNOPAUSE -dQUIET -dBATCH "-sOutputFile=${pdf_filename_base}-shrink.pdf" "${pdf_filename}"
}


# to alias d == fzf for directory
__fzf_mycd() {
  local dir
  dir=$(eval "${FZF_ALT_C_COMMAND}" 2> /dev/null | fzf +m) && cd "$dir"
}

# one letter aliases
alias n='nautilus . &'
alias d='__fzf_mycd'
alias o='fzf --bind "enter:execute(xdg-open {})"'
alias p='gopass_fzf.sh -c'
alias v='vim'

alias vim='nvim'
alias py='python3'
alias grep='grep --color'

# force TERM to xterm, so we do not have to push terminfo in the server environment
alias ssh='TERM=xterm ssh'

alias icat='kitty +kitten icat'

alias spek='mpv --pause --lavfi-complex="[aid1] showspectrumpic=size=2048x1024:color=rainbow:saturation=2:scale=log:win_func=bharris [vo]"'
