" VIM configuration file

" Now we set some defaults for the editor
set history=200		" keep 200 lines of command line history
set ruler		" show the cursor position all the time
set showcmd		" display incomplete commands
set wildmenu		" display completion matches in a status line
" modelines have historically been a source of security/resource
" vulnerabilities -- disable by default, even when 'nocompatible' is set
set nomodeline
" set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
" disable vi compatibility (emulation of old bugs)
set nocompatible
" turn syntax highlighting on
set t_Co=256
syntax on
" colorscheme wombat256
" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5

" Activate highlighting when searching
set hlsearch
" Use <Ctrl>-L (redraw screen shortcut) to also disable the existing
" highlight
nnoremap <C-L> :noh<cr><C-L> 
" Do incremental searching when it's possible to timeout.
if has('reltime')
  set incsearch
endif

" Mouse support
"
" if has('mouse')
"   set mouse=a
" endif
"
" NO MOUSE in normal and insert mode For f*** sake !
set mouse=ch

" bind VIM clipboard to the PRIMARY selection (mouse middle click)
" set clipboard=unnamed
" bind VIM clipboard to the CLIPBOARD buffer (Ctrl-C Ctrl-V)
set clipboard=unnamedplus


" https://github.com/junegunn/vim-plug
" Specify a directory for plugins
call plug#begin('~/.vim/plugged')

" Code completion
Plug 'Valloric/YouCompleteMe'
" conveniently show registers
Plug 'junegunn/vim-peekaboo'

" Initialize plugin system
call plug#end()

let g:peekaboo_window = 'vert bo 50new'
"let g:minimap_highlight='Visual'

" Additional YouCompleteMe configuration
let g:ycm_auto_trigger = 0
let g:ycm_confirm_extra_conf = 0
let g:ycm_goto_buffer_command = 'split-or-existing-window'
let g:ycm_autoclose_preview_window_after_insertion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
map <F5> :rightbelow vertical YcmCompleter GoToDefinition<CR>
map <F6> :rightbelow vertical YcmCompleter GoToDeclaration<CR>
map <F7> :rightbelow vertical YcmCompleter GoToInclude<CR>
map <F8> :YcmCompleter GetDoc<CR>

" Activate the filetype plugins.
" Vim will look at ~/.vim/ftplugin/<mime>.vim for each <mime> file extension
filetype plugin on


"" Check .git/tags for ctags file.
"fun! FindTagsFileInGitDir(file)
"  let path = fnamemodify(a:file, ':p:h')
"  while path != '/'
"    let fname = path . '/.git/tags'
"    if filereadable(fname)
"      silent! exec 'set tags+=' . fname
"    endif
"    let path = fnamemodify(path, ':h')
"  endwhile
"endfun
"
"augroup CtagsGroup
"  autocmd!
"  autocmd BufRead * call FindTagsFileInGitDir(expand("<afile>"))
"augroup END

"let Tlist_GainFocus_On_ToggleOpen=1

" keymaps to try later
"
" switch between header/source with F4
" map <F4> :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>
" recreate tags file with F5
" map <F5> :!ctags -R –c++-kinds=+p –fields=+iaS –extra=+q .<CR>
" create doxygen comment
" map <F6> :Dox<CR>
" build using makeprg with <F7>
" map <F7> :make<CR>
" build using makeprg with <S-F7>
" map <S-F7> :make clean all<CR>
" goto definition with F12
" map <F12> <C-]>
" in diff mode we use the spell check keys for merging
" if &diff
"   ” diff settings
"   map <M-Down> ]c
"   map <M-Up> [c
"   map <M-Left> do
"   map <M-Right> dp
"   map <F9> :new<CR>:read !svn diff<CR>:set syntax=diff buftype=nofile<CR>gg
" else
"   " spell settings
"   :setlocal spell spelllang=en
"   " set the spellfile - folders must exist
"   set spellfile=~/.vim/spellfile.add
"   map <M-Down> ]s
"   map <M-Up> [s
" endif
"

" Push preview window to the right and set width to 70
"augroup previewWindowPosition
"  au!
"  autocmd BufWinEnter * call PreviewWindowPosition()
"augroup END
"function! PreviewWindowPosition()
"  if &previewwindow
"    wincmd L
"    vertical resize 70
"    syn on
"  endif
"endfunction
"
"

" Automatically set the preview window height.
set previewheight=15

au BufEnter ?* call PreviewHeightWorkAround()

function! PreviewHeightWorkAround()
	if &previewwindow
		exec 'setlocal winheight='.&previewheight
	endif
endfunc




highlight Visual ctermfg=black ctermbg=green cterm=bold
