#!/bin/bash -x

# This script generates CTAGS in the .git/tags directory.

dir=`git rev-parse --git-dir`
rm -f "$dir/tags"
git ls-files | ctags --tag-relative -L - -f"$dir/tags"

