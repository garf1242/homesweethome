#!/usr/bin/env bash

shopt -s nullglob globstar

clipit=0
if [[ $1 == "-c" ]]; then
	clipit=1
fi

prefix=${PASSWORD_STORE_DIR:-~/.password-store}
password_files=( "$prefix"/**/*.gpg )
password_files=( "${password_files[@]#"$prefix"/}" )
password_files=( "${password_files[@]%.gpg}" )

password=$(printf '%s\n' "${password_files[@]}" | fzf)

[[ -n $password ]] || exit

if [[ $clipit -eq 1 ]]; then
	pass tailclip "$password"
else
	pass show "$password"
fi
