#!/usr/bin/env bash

shopt -s nullglob globstar

clipit=0
if [[ $1 == "-c" ]]; then
	clipit=1
fi

password=$(gopass ls --flat | fzf)

[[ -n $password ]] || exit

if [[ $clipit -eq 1 ]]; then
	gopass show -C "$password"
else
	gopass show "$password"
fi
