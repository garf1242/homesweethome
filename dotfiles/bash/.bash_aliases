alias ll='ls -alh'

alias vlc3='$HOME/workspace/videolan/build/vlc3/bin/vlc'
alias vlc4='$HOME/workspace/videolan/build/vlc4/bin/vlc'

alias docker-clean-containers='docker ps -aq --no-trunc | xargs docker rm'

alias scan-boulot='hp-scan -mcolor --size=a4'

alias pdf-shrink='gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4  -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf'

alias n='nautilus . &'
alias o='fzf --bind "enter:execute(xdg-open {})"'
alias v='fzf --bind "enter:execute(nvim {})"'
alias d='__fzf_mycd'
alias p='pass_fzf.sh'

alias vim='nvim'
