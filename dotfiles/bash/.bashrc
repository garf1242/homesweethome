# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth:erasedups

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=10000
HISTFILESIZE=10000

# need fonts-powerline package to work
PROMPT_COMMAND=__prompt_command # Func to gen PS1 after CMDs

__prompt_command() {

    local RCol='\[\e[0m\]'
    local Bold='\[\e[1m\]'
    local Defaultbg='\[\e[49m\]'

    local Red='\[\e[31m\]'
    local Green='\[\e[32m\]'
    local Lgray='\[\e[37m\]'
    local Yellow='\[\e[33m\]'
    local Dgraybg='\[\e[100m\]'
    local Blackbg='\[\e[40m\]'
    local Dgray='\[\e[90m\]'
    local White='\[\e[97m\]'
    local Lblue='\[\e[94m\]'
    local Lbluebg='\[\e[104m\]'
    local Black='\[\e[30m\]'
    local Blue25bg='\[\e[48;5;24m\]'
    local Blue25='\[\e[38;5;24m\]'
    local Lyellow='\[\e[93m\]'
    local Lred='\[\e[91m\]'
    local Gray235bg='\[\e[48;5;235m\]'
    local Gray235='\[\e[38;5;235m\]'
    local Gray238bg='\[\e[48;5;238m\]'
    local Gray238='\[\e[38;5;238m\]'
    local Red88='\[\e[38;5;88m\]'
    local Red88bg='\[\e[48;5;88m\]'

    # right align
    #tput sc;
    #printf "%*s" $COLUMNS "$(date +%T) "
    #tput rc;

    PS1="${Bold}"

    if [ "x${SSH_CLIENT}" != "x" ]; then
        PS1+="${Red88bg}${White} \h "
        # transition
        PS1+="${Blue25bg}${Red88}"$'\ue0b0'
    fi
    # who
    PS1+="${Blue25bg}${White} \u "
    # transition
    PS1+="${Gray238bg}${Blue25}"$'\ue0b0'
    # hostname
    #PS1+="${Gray235bg}${Red} \h "
    # transition
    #PS1+="${Gray238bg}${Gray235}"$'\ue0b0'
    # pwd
    PS1+="${Green} \w "
 
    local git_branch=`__git_ps1 '\ue0a0%s'`
    if [ "x$git_branch" != "x" ]; then
        # transition
        PS1+="${Gray235bg}${Gray238}"$'\ue0b0'
	# branch
        PS1+="${White}${Gray235bg} ${git_branch} ${Defaultbg}${Gray235}"$'\ue0b0'"${RCol} "
    else
        PS1+="${Defaultbg}${Gray238}"$'\ue0b0'"${RCol} "
    fi

    # update title
    echo -ne "\033]0;Terminal: ${short_pwd}\007"

    # Instant history save
    history -a;
}

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "${debian_chroot:-}" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# LS
# enable color support of ls and also add handy aliases
# -N is for literal quoting (no single quote wrapping)

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto -N'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    #alias grep='grep --color=auto'
    #alias fgrep='fgrep --color=auto'
    #alias egrep='egrep --color=auto'
fi

# colored GCC warnings and errors
#export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Just to modify so == socket which is used to color the prefix in completion (see colored-completion-prefix)
LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=0;94:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.Z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:"

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi

# Don't want to scale based on DPI, just on resolution
export QT_AUTO_SCREEN_SCALE_FACTOR=0

export PATH=${HOME}/bin:${PATH}
export HISTIGNORE="ls:ps:history"

# SSH and GPG
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)

export WORKSPACE=${HOME}/workspace
export LD_LIBRARY_PATH=/usr/local/lib/:/usr/lib


# Use fd (fdfind in debian) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
    fdfind --hidden --follow --exclude ".git" . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fdfind --type d --hidden --follow --exclude ".git" . "$1"
}

#FZF


export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden --follow -g "!{.git}/*" 2> /dev/null'
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_ALT_C_COMMAND="fdfind . -I -t d"
#source ${HOME}/utils/fzf/shell/completion.bash # https://github.com/junegunn/fzf
#source ${HOME}/utils/fzf/shell/key-bindings.bash

export FZF_DEFAULT_OPTS="--reverse --border --height=80%"
export FZF_CTRL_T_OPTS="--preview '
    _type=\$(file -b --mime-type {} | sed 's%/.*%%')
    case \${_type} in
    text)
        bat -n --color always {}
    ;;
    inode)
        ls -AlhN --color=always {}
    ;;
    image)
        termpix --max-width \${FZF_PREVIEW_COLUMNS} --height \${FZF_PREVIEW_LINES} --true-color {}
    ;;
    *)
	echo \${_type}
        file --mime-type {}
    ;;
    esac
'"
export FZF_ALT_C_OPTS="${FZF_CTRL_T_OPTS}"


# pass completion
_fzf_complete_pass() {
  _fzf_complete '+m' "$@" < <(
    pwdir=${PASSWORD_STORE_DIR-~/.password-store/}
    stringsize="${#pwdir}"
    find "$pwdir" -name "*.gpg" -print |
        cut -c "$((stringsize + 1))"-  |
        sed -e 's/\(.*\)\.gpg/\1/'
  )
}

# __fzf__mycd
__fzf_mycd() {
  local dir
  dir=$(eval "${FZF_ALT_C_COMMAND}" 2> /dev/null | fzf +m) && cd "$dir"
}


[ -n "$BASH" ] && complete -F _fzf_complete_pass -o default -o bashdefault pass


export EDITOR=nvim

# a directory alone is equivalent to cd <dir>
shopt -s autocd
# correct cd <dir with errors>
# shopt -s cdspell

# swaywm: fix java awt
export _JAVA_AWT_WM_NONREPARENTING=1
# android studio
android_studio_dir=$HOME/utils/android-studio
export PATH=$android_studio_dir/bin:$android_studio_dir/jre/bin:$PATH

