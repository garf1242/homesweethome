" Show git commit during interactive rebase in the preview window

function! PreviewCommitDuringRebase()
  try
    if mode() =~# "^[vV\<C-v>]"
      "do nothing in visual modes
      return
    endif
    if getline(".") =~ '^\s*#'
      throw "This is a comment line, do not preview"
    endif
    " get 2nd column == commit hash. Will fail if 2nd does not exists
    let hash = split(getline('.'))[1]
    let commitshow = systemlist("git show --no-color " . hash)
    if v:shell_error != 0
      throw "git show returned an error"
    endif
    " Launch preview edition
    silent execute "noautocmd pedit Commit " . hash 
    silent wincmd P  " jump to preview window
    if &previewwindow
      setlocal buftype=nofile " buffer should not be written
      setlocal syntax=git " colors == git
      wincmd L " put window to the right
      call setline(1, commitshow) " push command output to the buffer
      wincmd p	" back to old window
    endif
  catch
    " something gone wrong -> close preview
    silent! exe ":pclose!"
  endtry
endfunction

" Inside the `git-rebase-todo` file, try to launch the preview on cursor
" movement
:autocmd CursorMoved,CursorMovedI */.git/rebase-merge/git-rebase-todo :call PreviewCommitDuringRebase()
