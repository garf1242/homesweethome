" Show staged changes during interactive commit message edition

function! PreviewCommitStaged()
  try
    if mode() =~# "^[vV\<C-v>]"
      "do nothing in visual modes
      return
    endif
    let stagedshow = systemlist("git diff --staged --no-color")
    if v:shell_error != 0
      throw "git show returned an error"
    endif
    " Launch preview edition
    silent execute "noautocmd pedit Staged changes"
    silent wincmd P  " jump to preview window
    if &previewwindow
      setlocal buftype=nofile " buffer should not be written
      setlocal syntax=git " colors == git
      wincmd L " put window to the right
      call setline(1, stagedshow) " push command output to the buffer
      wincmd p	" back to old window
    endif
  catch
    " something gone wrong -> close preview
    silent! exe ":pclose!"
  endtry
endfunction

" On commit message editing, try to launch the staged changes
:autocmd CursorMoved,CursorMovedI */.git/COMMIT_EDITMSG :call PreviewCommitStaged()
