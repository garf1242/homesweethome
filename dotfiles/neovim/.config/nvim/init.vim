" VIM configuration file

" Now we set some defaults for the editor
" modelines have historically been a source of security/resource
" vulnerabilities -- disable by default, even when 'nocompatible' is set
set nomodeline
" Show a few lines of context around the cursor.  Note that this makes the
" text scroll if you mouse-click near the start or end of the window.
set scrolloff=5

" no folding
set nofoldenable

" Use <Ctrl>-L (redraw screen shortcut) to also disable the existing
" highlight
nnoremap <C-L> :noh<cr><C-L> 

" Mouse support
"
" if has('mouse')
"   set mouse=a
" endif
"
" NO MOUSE For f*** sake !
set mouse=

" bind VIM clipboard to the PRIMARY selection (mouse middle click)
" set clipboard=unnamed
" bind VIM clipboard to the CLIPBOARD buffer (Ctrl-C Ctrl-V)
set clipboard=unnamedplus

" Search
set incsearch       " search as characters are entered
set hlsearch        " highlight matches
set ignorecase      " ignore case when searching
set smartcase       " ignore case if search pattern is lower case
                    " case-sensitive otherwise
" when spliting, put the new buffer below
set splitbelow
" when vspliting, put the new buffer on the right
set splitright

" https://github.com/junegunn/vim-plug
" Specify a directory for plugins
call plug#begin('~/.config/nvim/plugged')

" Golang Code formating
" Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
" Golang completion
" Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Fzf integration
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
" Indent guides
Plug 'nathanaelkane/vim-indent-guides'
" ANSI escape sequence
Plug 'powerman/vim-plugin-AnsiEsc'

" Nvim LSP
Plug 'neovim/nvim-lspconfig' " Collection of configurations for built-in LSP client
" Autocompletion
Plug 'hrsh7th/cmp-nvim-lsp' " LSP source for nvim-cmp
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/nvim-cmp' " Autocompletion plugin
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
" vsnip (snippets)
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

" French Grammar check
Plug 'dpelle/vim-Grammalecte'

" Treesitter
" https://github.com/nvim-treesitter/nvim-treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Initialize plugin system
call plug#end()

" lsp/autocomplete configuration
runtime lsp.vim

" treesitter confituration
runtime treesitter.vim

" STYLE/Fashion victim section

" Use the GUI colors in TUI - so activate truecolors if the terminal is
" compatible.
set termguicolors
" Use default theme
colorscheme default
" Change status line color
" highlight StatusLine ctermfg=24 ctermbg=white guifg=#004455 guibg=white
highlight StatusLineNC guibg=#004455 guifg=white gui=None
highlight StatusLine guibg=#aaeeff guifg=black gui=None
highlight VertSplit guibg=black guifg=#aaeeff gui=None
" Remove any bold style from default GUI colors 
highlight Comment	guifg=Cyan gui=None
highlight Constant	guifg=Violet gui=None
highlight Identifier	gui=None
highlight Statement	gui=None
highlight PreProc	gui=None
highlight Type		gui=None
highlight Special	gui=None
"hi Underlined  gui=None
highlight Ignore	gui=None
"hi Error       gui=None
highlight Todo		gui=None

" Make Popup menu semi-transparent.
set pumblend=10
" Change completion/highlight colors
highlight Pmenu cterm=NONE ctermbg=24 ctermfg=15 gui=NONE guibg=#004455 guifg=#eeeeee
highlight PmenuSel cterm=NONE ctermbg=231 ctermfg=160 gui=NONE guibg=#ffffff guifg=#d70000
" highlight PmenuSbar cterm=NONE ctermbg=231 ctermfg=160 gui=NONE guibg=#ffffff guifg=#d70000
" highlight PmenuThumb cterm=NONE ctermbg=231 ctermfg=160 gui=NONE guibg=#ffffff guifg=#d70000

" nvim-cmp menu colors
" gray
highlight CmpItemAbbrDeprecated guibg=NONE gui=strikethrough guifg=#808080
" blue
highlight CmpItemAbbrMatch guibg=NONE guifg=#569CD6
highlight CmpItemAbbrMatchFuzzy guibg=NONE guifg=#569CD6
" light blue
highlight CmpItemKindVariable guibg=NONE guifg=#9CDCFE
highlight CmpItemKindInterface guibg=NONE guifg=#9CDCFE
highlight CmpItemKindText guibg=NONE guifg=#9CDCFE
" pink
highlight CmpItemKindFunction guibg=NONE guifg=#C586C0
highlight CmpItemKindMethod guibg=NONE guifg=#C586C0
" front
highlight CmpItemKindKeyword guibg=NONE guifg=#D4D4D4
highlight CmpItemKindProperty guibg=NONE guifg=#D4D4D4
highlight CmpItemKindUnit guibg=NONE guifg=#D4D4D4


" fzf custom configuration
nnoremap <C-p> :Files<CR>
nnoremap <C-g> :Rg<CR>
let g:fzf_layout = { 'down': '80%' }

" indent guides configuration
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_auto_colors = 0
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=234 guibg=#202020
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=235 guibg=#191919
let g:indent_guides_guide_size = 1

" Vim will look at ~/.vim/ftplugin/<mime>.vim for each <mime> file extension

" Automatically set the preview window height.
set previewheight=15

au BufEnter ?* call PreviewHeightWorkAround()

function! PreviewHeightWorkAround()
	if &previewwindow
		exec 'setlocal winheight='.&previewheight
	endif
endfunc

" Flashy Visual 
highlight Visual ctermfg=black ctermbg=green cterm=bold guifg=black guibg=lightgreen gui=bold

" spell languages
set spelllang=fr
" Ctrl+S in normal mode to toggle spell check
" Ctrl+X S or Ctrl+X Ctrl+S to get suggestion
nnoremap <silent> <C-s> :set spell!<cr>
" Grammalecte
" :GrammalecteCheck to launch the check
" :GrammalecteClear to clear the check
let g:grammalecte_cli_py="/usr/bin/grammalecte-cli"

" use Shift+Up/Down to move current line or visual selection up or down
" cf. https://vimtricks.com/p/vimtrick-moving-lines/
" NOTE: do not filter the selected lines (removing ==), to avoid
" auto-indentation mess
nnoremap <S-Down> :m .+1<CR>
nnoremap <S-Up> :m .-2<CR>
inoremap <S-Down> <Esc>:m .+1<CR>gi
inoremap <S-Up> <Esc>:m .-2<CR>gi
vnoremap <S-Down> :m '>+1<CR>gv
vnoremap <S-Up> :m '<-2<CR>gv
" nnoremap <S-Down> :m .+1<CR>==
" nnoremap <S-Up> :m .-2<CR>==
" inoremap <S-Down> <Esc>:m .+1<CR>==gi
" inoremap <S-Up> <Esc>:m .-2<CR>==gi
" vnoremap <S-Down> :m '>+1<CR>gv=gv
" vnoremap <S-Up> :m '<-2<CR>gv=gv

runtime gitrebase.vim
runtime gitcommitpreview.vim

" Enables cursor line position tracking
set cursorline
" Removes the underline causes by enabling cursorline:
highlight clear CursorLine
" Sets the line numbering in bold color and the rest in darkgrey
highlight LineNR ctermfg=black guifg=#666666 guibg=black
highlight CursorLineNR ctermfg=white cterm=bold guifg=white gui=bold
" Diagnostics/Sign column
highlight SignColumn guibg=black
