" Neovim native LSP and Autocompletion configuration

" nvim LSP config
lua << EOF

-- 
--   -- Use an on_attach function to only map the following keys
--   -- after the language server attaches to the current buffer
--   local on_attach = function(client, bufnr)
--     local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
--     local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
--   
--     -- Enable completion triggered by <c-x><c-o>
--     buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
--   
--     -- Mappings.
--     local opts = { noremap=true, silent=true }
--   
--     -- See `:help vim.lsp.*` for documentation on any of the below functions
--     buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
--     buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
--     buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
--     buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
--     buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
--     buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
--     buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
--     buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
--     buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
--     buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
--     buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
--     buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
--     buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
--     buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
--     buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
--     buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
--     buf_set_keymap('n', '<space>f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)
-- 
-- 
--     -- define new signs for diagnostics
--     local signs = { Error = "🔴", Warn = "⚡", Hint = "💡", Info = "💬" }
--     for type, icon in pairs(signs) do
--       local hl = "LspDiagnosticsSign" .. type
--       vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
--     end
-- 
--     -- Define LSP diagnostic default styles (both sign and text)
--     vim.cmd [[
--       highlight LspDiagnosticsDefaultError ctermfg=red cterm=bold guifg=red gui=bold
--       highlight LspDiagnosticsDefaultWarning guifg=yellow gui=bold guifg=yellow gui=bold 
--       highlight LspDiagnosticsDefaultInformation guifg=green gui=bold guifg=green gui=bold
--       highlight LspDiagnosticsDefaultHint guifg=blue gui=bold guifg=blue gui=bold
--       " Sign column should be in black bg
--       highlight SignColumn ctermbg=black guibg=black
--       ]]
--   end
-- 
--   -- enable vscode html/css
--   local capabilities = vim.lsp.protocol.make_client_capabilities()
--   capabilities.textDocument.completion.completionItem.snippetSupport = true
--   require'lspconfig'.html.setup{
--       on_attach = on_attach,
--       capabilities = capabilities,
--       filetypes = { "css", "html" }
--   }
--   -- enable tsserver (javascript)
--   require'lspconfig'.tsserver.setup{on_attach = on_attach}
-- 
--   -- enable rust analyzer
--   require'lspconfig'.rust_analyzer.setup{on_attach = on_attach}
-- 
--   -- enable python lsp (pyright)
--   -- require'lspconfig'.pyright.setup{}
-- 
EOF


lua <<EOF

  -- Set completeopt to have a better completion experience
  vim.o.completeopt = 'menuone,noselect,noinsert'

  -- define new signs for diagnostics
  local signs = { Error = "🔴", Warn = "⚡", Hint = "💡", Info = "💬" }
  for type, icon in pairs(signs) do
    local hl = "DiagnosticSign" .. type
    vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
  end

  -- Setup nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
      end,
    },
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      -- ['<Up>'] = cmp.mapping.select_prev_item(),
      -- ['<Down>'] = cmp.mapping.select_next_item(),
      -- ['<CR>'] = cmp.mapping.confirm {
      --  behavior = cmp.ConfirmBehavior.Replace,
      --  select = true,
      --},
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      { name = 'vsnip' }, -- For vsnip users.
    }, {
      { name = 'buffer' },
    })
  })

  -- Set configuration for specific filetype.
  -- cmp.setup.filetype('gitcommit', {
  --   sources = cmp.config.sources({
  --     { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
  --   }, {
  --     { name = 'buffer' },
  --   })
  -- })

  -- -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  -- cmp.setup.cmdline('/', {
  --   mapping = cmp.mapping.preset.cmdline(),
  --   sources = {
  --     { name = 'buffer' }
  --   }
  -- })

  -- -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  -- cmp.setup.cmdline(':', {
  --   mapping = cmp.mapping.preset.cmdline(),
  --   sources = cmp.config.sources({
  --     { name = 'path' }
  --   }, {
  --     { name = 'cmdline' }
  --   })
  -- })

  -- Setup lspconfig.
  local capabilities = require('cmp_nvim_lsp').default_capabilities()
  capabilities.textDocument.completion.completionItem.snippetSupport = true

  -- LSP Servers config

  -- debug
  -- vim.lsp.set_log_level("debug")

  function lsp_keymap(bufnr)
    local bufopts = { noremap=true, silent=true, buffer=bufnr }
    vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
    vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
    vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
    vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
    vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
    vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
    vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
    vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
    vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  end

  -- Golang
  require('lspconfig')['gopls'].setup{
    capabilities = capabilities,
    settings = {
      gopls = {
        analyses = {
          unusedparams = true,
          shadow = true,
        },
        staticcheck = true,
      },
    },
    init_options = {
      usePlaceholders = true,
    },
    on_attach = function(client , bufnr)
      lsp_keymap(bufnr)
    end
  }
  -- Auto-format *.go (golang) files prior to saving them
  vim.api.nvim_command('autocmd BufWritePre *.go lua vim.lsp.buf.format()')

  -- Python
  require('lspconfig')['pylsp'].setup{ capabilities = capabilities }

  -- Web
  -- need to install vscode-langservers-extracted (yay on arch)
  require('lspconfig')['cssls'].setup { capabilities = capabilities }

EOF

