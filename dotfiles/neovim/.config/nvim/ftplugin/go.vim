
" VIM configuration file for go language

" use indentation of previous line
set autoindent
" use intelligent indentation
set smartindent
" turn line numbers on
set number
" highlight matching braces
set showmatch
" intelligent comments
set comments=sl:/*,mb:\ *,elx:\ */

