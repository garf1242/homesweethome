" VIM configuration file for HTML language

" use indentation of previous line
set autoindent
" use intelligent indentation for HTML
set smartindent
" configure tabwidth and insert spaces instead of tabs
set tabstop=2        " tab width is 4 spaces
set shiftwidth=2     " indent also with 4 spaces
set expandtab        " expand tabs to spaces
" turn line numbers on
set number
" highlight matching braces
set showmatch

