" VIM configuration file for Shell scripts

" use indentation of previous line
set autoindent
" configure tabwidth and insert spaces instead of tabs
set tabstop=4        " tab width is 4 spaces
set shiftwidth=4     " indent also with 4 spaces
set expandtab        " expand tabs to spaces
" turn line numbers on
set number
" highlight matching braces
set showmatch

