
Configure Yubikey in challenge-response with keepassxc

- install Yubikey Personalization tool (gui)
- Select:
  - Challenge-Response
  - HMAC-SHA1
  - Slot 2
  - Require user input (button press)
  - Fixed 64 byte input
    - generate the 20 bytes values
    - save them IRL!
    - save the slot 2

