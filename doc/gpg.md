# Installing GPG/Yubikey

To setup the yubikey: https://github.com/drduh/YubiKey-Guide

- install packages: gnupg2 gnupg-agent scdaemon pcscd
- import pubkey
```
$ gpg --import pubkey.txt # import key
$ gpg --edit-key <KEYID> # to trust it
> trust
... 5 (ultimately)
```

Check:
- remove and re-insert yubikey
```
$ gpg --card-status
```

