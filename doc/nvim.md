# Neovim

## why switching to neovim ?

- 90% of my vim configuration is default of neovim
- clipboard management with VIM + wayland is nightmare

## What to do other than installing configuration files

- create the nvim configuration directory before to let nvim generate things in it:
  ```shell
  $ mkdir -p ~/.config/nvim
  ```
- install [vim-plug](https://github.com/junegunn/vim-plug)

