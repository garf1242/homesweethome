# Nix cheatsheet

## List all packages available

`nix-env -qaP`

## Install a package

`nix-env -i <package>`

## Install from a nix file

`nix-env -if <file>.nix`

## Update environment

`nix-env -u`

## Get package information

`nix-env -qa --description --json <package>`


