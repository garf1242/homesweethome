# Zsh configuration


## Requirements

Packages:
```shell
apt install zsh zsh-syntax-highlighting zsh-autosuggestions fzf ripgrep fd-find
```

Theme:
```
~/homesweethome/$ stow zsh
~/$ fc-cache -f -v 
```

Then Go to the terminal settings, and in profile, choose the `MesloLGS NF Regular` font.

