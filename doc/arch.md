# Changes from Standard Arch installation guide

install essential packages:
- base linux linux-firmware lvm2 iwd networkmanager neovim git efibootmgr

## mount points

### UEFI

- Mounting ESP partition to /efi

This will avoid removing any other OS by accident

esp partition -> /efi

- mounting /efi/EFI/arch to /boot

/etc/fstab:
```
/efi/EFI/arch	/boot	none	defaults,bind 0 0
```

linux package will update linux image in /boot by default, so it should do the trick

## Boot

### initramfs

you probably need to add lvm2 in mkinitcpio.conf HOOKS.

### Adding an entry in machine EFI:

- Get the UUID from `/etc/fstab` or `blkid` command

- use the following command:

```
efibootmgr --disk /dev/sdb --part 1 --create --label "Arch Linux" --loader /EFI/arch/vmlinuz-linux --unicode "root=UUID=878991dd-425e-457f-bd58-54d45ec66a20 rw initrd=\EFI\arch\initramfs-linux.img" --verbose
```

NOTE: Yes, the EFI params should have backslashes for paths...

## Network

### using iwd

during installation

`pacman -S iwd`

after installation

`systemctl enable iwd`

- using iwd dhcp + systemd resolved

/etc/iwd/main.conf:
```
[General]
EnableNetworkConfiguration=true

[Network]
NameResolvingService=systemd
```

and 

`systemctl enable systemd-resolved`

### NetworkManager

follow arch wiki


## Users

```
groupadd --gid 1000 garfvl
useradd --home-dir /home/garfvl --gid garfvl --no-create-home --shell /bin/zsh --uid 1000 garfvl
groupadd --gid 1001 garf
useradd --home-dir /home/garf --gid garf --no-create-home --shell /bin/bash --uid 1001 garf
passwd garfvl
passwd garf
```

### permissions

install sudo and configure it


## GI

- for now, install gnome/gdm for login page and some other utilities (nautilus/gvfs, etc.). Should try to find some lighter replacement later
- enable gdm service

### Other

- Install yay
- install aur/ttf-meslo-nerd-font-powerlevel10k


### Rootless docker

Scripts will install docker globally on the system (`pacman -S docker`).

To switch to a rootless setup:

#### disable the global service/socket

```
sudo systemctl disable --now docker.service docker.socket
```

#### Edit subuid/subgid

- edit both `/etc/subuid` and `/etc/subgid`:

```
<login>:<starting uid>:65536
```

example: `myuser:100000:65536`

#### Install docker rootless extra tools

```
pacman -S slirp4netns # network sandbox
yay -S docker-rootless-extras 
```

#### Get user script configuration and launch it

```
wget https://raw.githubusercontent.com/moby/moby/master/contrib/dockerd-rootless-setuptool.sh
./dockerd-rootless-setuptool.sh install --force
```

#### Then enable the docker user service

```
systemctl --user enable docker
```

### TODO

- Don't forget to install firefox containers extension


## Tips

### Troubleshooting

In case of boot break/linux package issue:

- boot on a arch install USB key
- type `fdisk -l` to get the arch root partition
- mount the root partition: `mount <arch_root> /mnt`
- launch chroot: `arch-chroot /mnt`
- mount efi+boot: `mount /efi; mount /boot`
- now you're good to go

